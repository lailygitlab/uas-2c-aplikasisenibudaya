package nuraini.laily.appsenibudaya

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_tari.*

class AdapterTari (
    val dataTari : List<HashMap<String, String>>,
    val activityTari: ActivityTari) :
    RecyclerView.Adapter<AdapterTari.HolderTari>() {

    class HolderTari(v : View) : RecyclerView.ViewHolder(v){
        val txId_Tari = v.findViewById<TextView>(R.id.txId_Tari)
        val txNamaTarian = v.findViewById<TextView>(R.id.txNamaTarian)
        val txDaerah = v.findViewById<TextView>(R.id.txDaerah)
        val txSejarah = v.findViewById<TextView>(R.id.txSejarah)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val CLayout1 = v.findViewById<ConstraintLayout>(R.id.CLayout)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterTari.HolderTari {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_tari,p0, false)
        return HolderTari(v)
    }

    override fun getItemCount(): Int {
        return dataTari.size
    }

    override fun onBindViewHolder(p0: AdapterTari.HolderTari, p1: Int) {
        val data = dataTari.get(p1)
        p0.txId_Tari.setText(data.get("id_tari"))
        p0.txNamaTarian.setText(data.get("namatarian"))
        p0.txDaerah.setText(data.get("daerah"))
        p0.txSejarah.setText(data.get("sejarah"))

        if(p1.rem(2) == 0) p0.CLayout1.setBackgroundColor(
            Color.rgb(230,245,240))
        else p0.CLayout1.setBackgroundColor(Color.rgb(255,255,245))

        p0.CLayout1.setOnClickListener({
            val pos = activityTari.daftarDaerah.indexOf(data.get("daerah"))
            activityTari.spdaerah.setSelection(pos)
            activityTari.edId_tari.setText(data.get("id_tari"))
            activityTari.ed_namatari.setText(data.get("namatarian"))
            activityTari.edSejarah.setText(data.get("sejarah"))
            Picasso.get().load(data.get("url")).into(activityTari.imgUploade)

        })

        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }
}