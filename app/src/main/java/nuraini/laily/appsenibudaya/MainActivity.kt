package nuraini.laily.appsenibudaya

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var preferences: SharedPreferences
    val PREF_NAME = "setting"
    val DEFF_IDENTY = "LAILY LELY"
    val DEF_JUDUL = "SENI BUDAYA JAWA TIMUR !!!"
    val TITLE_IDENTY = ""
    val FONT_TITLE = "F"
    val DEF_FONTHEAD_SIZE = 20
    val TITLE_JUDUL = ""
    val FONT_JUDUL = "G"
    val DEF_FONT_SIZE = 20

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.optionalmenu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        txTitle.setText(preferences.getString(TITLE_IDENTY,DEFF_IDENTY))
        txTitle.setTextSize(preferences.getInt(FONT_TITLE,DEF_FONT_SIZE).toFloat())
        txHeader.setText(preferences.getString(DEF_JUDUL,DEF_JUDUL))
        txHeader.setTextSize(preferences.getInt(FONT_JUDUL,DEF_FONTHEAD_SIZE).toFloat())


        btnTari.setOnClickListener(this)
        btnBudaya.setOnClickListener(this)
        btnVideo.setOnClickListener(this)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){

            R.id.btnVideo ->{
                var video = Intent(this,ActivityVideo::class.java)
                startActivity(video)
                true
            }
            R.id.btnTari ->{
                var tari = Intent(this,ActivityTari::class.java)
                startActivity(tari)
                true
            }
            R.id.btnBudaya ->{
                var budaya = Intent(this,ActivityBudaya::class.java)
                startActivity(budaya)
                true
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.itemSetting -> {
                var intent = Intent(this,SettingActivity::class.java)
                startActivity(intent)

            }
        }

        return super.onOptionsItemSelected(item)
    }


}