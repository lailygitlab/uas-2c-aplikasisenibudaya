package nuraini.laily.appsenibudaya

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_budaya.*

class AdapterBudaya (
    val dataBudaya : List<HashMap<String, String>>,
    val activityBudaya: ActivityBudaya) :
    RecyclerView.Adapter<AdapterBudaya.HolderBudaya>() {

    class HolderBudaya(v: View) : RecyclerView.ViewHolder(v) {
        val txId_Budaya = v.findViewById<TextView>(R.id.txId_Budaya)
        val txNamaBudaya = v.findViewById<TextView>(R.id.txNamaBudaya)
        val txDaerah = v.findViewById<TextView>(R.id.txDaerah)
        val txSejarah = v.findViewById<TextView>(R.id.txSejarah)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val CLayout = v.findViewById<ConstraintLayout>(R.id.CLayout)
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterBudaya.HolderBudaya {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_budaya, p0, false)
        return HolderBudaya(v)
    }

    override fun getItemCount(): Int {
        return dataBudaya.size
    }

    override fun onBindViewHolder(p0: AdapterBudaya.HolderBudaya, p1: Int) {
        val data = dataBudaya.get(p1)
        p0.txId_Budaya.setText(data.get("id_budaya"))
        p0.txNamaBudaya.setText(data.get("namabudaya"))
        p0.txDaerah.setText(data.get("daerah"))
        p0.txSejarah.setText(data.get("sejarah"))

        if (p1.rem(2) == 0) p0.CLayout.setBackgroundColor(
            Color.rgb(230, 245, 240)
        )
        else p0.CLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.CLayout.setOnClickListener({
            val pos = activityBudaya.daftarDaerah.indexOf(data.get("daerah"))
            activityBudaya.spdrh.setSelection(pos)
            activityBudaya.edId_budaya.setText(data.get("id_budaya"))
            activityBudaya.ed_namabudaya.setText(data.get("namabudaya"))
            activityBudaya.edSjrh.setText(data.get("sejarah"))
            Picasso.get().load(data.get("url")).into(activityBudaya.imgUploade)

        })

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo)
    }
}