package nuraini.laily.appsenibudaya

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_budaya.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class ActivityBudaya:AppCompatActivity(), View.OnClickListener  {
    lateinit var mediaHelper: MediaHelper
    lateinit var budayaAdapter : AdapterBudaya
    lateinit var daerahAdapter: ArrayAdapter<String>
    var daftarBudaya = mutableListOf<HashMap<String,String>>()
    var daftarDaerah = mutableListOf<String>()
    val url = "http://192.168.1.6/UAS-2C-AplikasiSeniBudaya-Web/show_budaya.php"
    var url2 = "http://192.168.1.6/UAS-2C-AplikasiSeniBudaya-Web/show_daerah.php"
    var url3 = "http://192.168.1.6/UAS-2C-AplikasiSeniBudaya-Web/query_ins_up_del_budaya.php"
    var imStr = ""
    var pilihDaerah = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_budaya)
        budayaAdapter = AdapterBudaya(daftarBudaya,this)
        mediaHelper = MediaHelper(this)
        lsBudaya.layoutManager = LinearLayoutManager(this)
        lsBudaya.adapter = budayaAdapter

        daerahAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarDaerah)
        spdrh.adapter = daerahAdapter
        spdrh.onItemSelectedListener = itemSelected

        imgUploade.setOnClickListener(this)
        btInsert.setOnClickListener(this)
        btUpdate.setOnClickListener(this)
        btDelete.setOnClickListener(this)
        btnFind.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUploade -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btInsert -> {
                queryInsertUpdateDelete("insert")
            }
            R.id.btDelete -> {
                queryInsertUpdateDelete("delete")
            }
            R.id.btUpdate -> {
                queryInsertUpdateDelete("update")
            }
            R.id.btnFind -> {
                showDataBudaya(ed_namabudaya.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spdrh.setSelection(0)
            pilihDaerah = daftarDaerah.get(0)
        }

        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihDaerah = daftarDaerah.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if (requestCode == mediaHelper.getRcGallery()) {
                imStr = mediaHelper.getBitmapToString(data!!.data, imgUploade)
            }
        }
    }
    fun queryInsertUpdateDelete(mode : String) {
        val request = object : StringRequest(
            Method.POST, url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")) {
                    Toast.makeText(this, "Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataBudaya("")
                } else {
                    Toast.makeText(this, "Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                val nmFile = "DC" + SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date()) + ".jpg"
                when (mode) {
                    "insert" -> {
                        hm.put("mode", "insert")
                        hm.put("id_budaya", edId_budaya.text.toString())
                        hm.put("namabudaya", ed_namabudaya.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("daerah", pilihDaerah)
                        hm.put("sejarah", edSjrh.text.toString())
                    }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("id_budaya", edId_budaya.text.toString())
                        hm.put("namabudaya", ed_namabudaya.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                        hm.put("daerah", pilihDaerah)
                        hm.put("sejarah", edSjrh.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("id_film", edId_budaya.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaDaerah(namaDaerah : String) {
        val request = object : StringRequest(Request.Method.POST, url2,
            Response.Listener { response ->
                daftarDaerah.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarDaerah.add(jsonObject.getString("daerah"))
                }
                daerahAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this, "Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG)
                    .show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("daerah", namaDaerah)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataBudaya(namaBudaya : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarBudaya.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length() - 1)) {
                    val jsonObject = jsonArray.getJSONObject(x)
                    var budaya = HashMap<String, String>()
                    budaya.put("id_budaya", jsonObject.getString("id_budaya"))
                    budaya.put("namabudaya", jsonObject.getString("namabudaya"))
                    budaya.put("daerah", jsonObject.getString("daerah"))
                    budaya.put("sejarah", jsonObject.getString("sejarah"))
                    budaya.put("url", jsonObject.getString("url"))
                    daftarBudaya.add(budaya)
                }
                budayaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }) {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("namabudaya", namaBudaya)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataBudaya("")
        getNamaDaerah("")
    }
}